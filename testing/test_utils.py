"""Contains class to test data.utils module."""

import unittest
import data.utils as utils
import data.study_set as study_set


class TestUtils(unittest.TestCase):
    """Test the methods in the data.utils module."""
    
    def test_get_json_files(self):
        """Test that get_json_files properly 
           returns a list of json file names 
           when passed a list of various file types."""
        file_list = ["valid.json", "bad.txt", "horrible.py", "no"]
        json_list = utils.get_json_files(file_list)
        expected_json_list = [file_list[0]]
        self.assertListEqual(json_list, expected_json_list)
    
    def test_study_set_name_list(self):
        """Test that study_set_name_list properly takes json files 
           and converts them to study set names 
           when passed a list of various file types.
        """
        file_list = ["valid.json", "longer_valid.json", 
                     "very_long_study_set_name.json", "invalid.py"]
        study_set_list = utils.study_set_name_list(file_list)
        expected_list = ["valid", "longer valid", 
                         "very long study set name"]
        self.assertListEqual(study_set_list, expected_list)
    
    def test_card_list_to_dictionary(self):
        """Test that card_list_to_dictionary properly converts
           a list of cards to a dictionary where the card front 
           becomes the key and the card back becomes the value.
        """
        cards = [study_set.Card("Front1", "Back1"),
                 study_set.Card("Front2", "Back2"),
                 study_set.Card("Front3", "Back3")]
        dictionary = utils.card_list_to_dictionary(cards)
        expected = {"Front1": "Back1", "Front2": "Back2", "Front3": "Back3"}
        self.assertDictEqual(dictionary, expected)
    
    def test_dictionary_to_card_list(self):
        """Test that dictionary_to_card_list properly converts
           a dictionary to a list of cards where the key becomes
           the card fron and the value becomes the card back.
        """
        dictionary = {"Front1": "Back1", "Front2": "Back2", "Front3": "Back3"}
        cards = utils.dictionary_to_card_list(dictionary)
        self.assertEqual(len(cards), 3)
        for card in cards:
            self.assertTrue(card.front in dictionary)
            self.assertTrue(card.back == dictionary[card.front])
    
    def test_json_filename(self):
        """Test that json_filename properly converts a string
           to the expected json file name.
        """
        stub = "my file"
        filename = utils.json_filename(stub)
        expected = "study_sets/my_file.json"
        self.assertEqual(filename, expected)