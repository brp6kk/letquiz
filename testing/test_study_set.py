"""Contains class to test data.study_set module."""

import unittest
import data.study_set as study_set


class TestStudySet(unittest.TestCase):
    """Test the methods in the data.study_set module."""
    
    def test_card_init(self):
        """Test that Card init properly sets attributes."""
        card = study_set.Card("front", "back")
        self.assertEqual(card.front, "front")
        self.assertEqual(card.back, "back")
    
    def test_study_set_init(self):
        """Test that StudySet init properly sets attributes."""
        cards = study_set.StudySet("My set")
        self.assertEqual(cards.name, "My set")
        self.assertListEqual(cards.cards, [])
        self.assertIsNone(cards.current_card_index)
    
    def test_study_set_add_card(self):
        """Test that StudySet's add_card method 
           properly adds cards to the set.
        """
        to_add = study_set.Card("front", "back")
        cards = study_set.StudySet("My set")
        cards.add_card(to_add)
        self.assertEqual(len(cards.cards), 1)
        self.assertListEqual(cards.cards, [to_add])
        self.assertEqual(cards.current_card_index, 0)
        
    def test_study_set_remove_card(self):
        """Test that StudySet's remove_card method
           properly removes a card from the set.
        """
        to_add = [study_set.Card("front1", "back1"),
                  study_set.Card("front2", "back2")]
        cards = study_set.StudySet("My set")
        for card in to_add:
            cards.add_card(card)
        
        cards.remove_card(to_add[0])
        self.assertEqual(len(cards.cards), 1)
        self.assertListEqual(cards.cards, [to_add[1]])
        self.assertEqual(cards.current_card_index, 0)
        
        cards.remove_card(to_add[1])
        self.assertEqual(len(cards.cards), 0)
        self.assertIsNone(cards.current_card_index)
    
    def test_study_set_remove_card_failure(self):
        """Test that StudySet's remove_card method
           properly raises a ValueError when attempting
           to remove a card not in the set.
        """
        to_add = study_set.Card("front", "back")
        to_remove = study_set.Card("back", "front")
        cards = study_set.StudySet("My set")
        cards.add_card(to_add)
        
        try:
            cards.remove_card(to_remove)
            value = False
        except ValueError:
            value = True
        self.assertTrue(value)
    
    def test_study_set_edit_card(self):
        """Test that StudySet's edit_card method
           properly changes one card to another.
        """
        to_add = study_set.Card("front", "back")
        change_to = study_set.Card("new front", "new back")
        cards = study_set.StudySet("My set")
        cards.add_card(to_add)
        
        cards.edit_card(to_add, change_to)
        self.assertEqual(len(cards.cards), 1)
        self.assertListEqual(cards.cards, [change_to])
    
    def test_study_set_edit_card_failure(self):
        """Test that StudySet's edit_card method
           properly does nothing when the old card
           does not exist in the set.
        """
        to_add = study_set.Card("front", "back")
        nonexistent = study_set.Card("not front", "not back")
        change_to = study_set.Card("new front", "new back")
        cards = study_set.StudySet("My set")
        cards.add_card(to_add)
        
        cards.edit_card(nonexistent, change_to)
        self.assertEqual(len(cards.cards), 1)
        self.assertListEqual(cards.cards, [to_add])
    
    def test_study_set_find_card_front(self):
        """Test that StudySet's find_card method
           properly returns a card when provided with
           a valid "front" value.
        """
        to_add = study_set.Card("front", "back")
        cards = study_set.StudySet("My set")
        cards.add_card(to_add)
        
        check = cards.find_card(front="front")
        self.assertEqual(check, to_add)
    
    def test_study_set_find_card_back(self):
        """Test that StudySet's find_card method
           properly returns a card when provided with
           a valid "back" value.
        """
        to_add = study_set.Card("front", "back")
        cards = study_set.StudySet("My set")
        cards.add_card(to_add)
        
        check = cards.find_card(back="back")
        self.assertEqual(check, to_add)
    
    def test_study_set_find_card_failures(self):
        """Test that StudySet's find_card method
           properly returns None when:
           - Provided with neither a front nor back value
           - Provided with a front value that does not exist
           - Provided with a back value that does not exist
        """
        to_add = study_set.Card("front", "back")
        cards = study_set.StudySet("My set")
        cards.add_card(to_add)
        
        check = cards.find_card()
        self.assertIsNone(check)
        
        check = cards.find_card(front="not front")
        self.assertIsNone(check)
        
        check = cards.find_card(back="not back")
        self.assertIsNone(check)
    
    def test_study_set_get_top_card(self):
        """Test that StudySet's get_top_card method
           properly returns different cards in the expected order
           when the set contains multiple cards.
        """
        to_add = [study_set.Card("front1", "back1"),
                  study_set.Card("front2", "back2"),
                  study_set.Card("front3", "back3")]
        cards = study_set.StudySet("My set")
        for card in to_add:
            cards.add_card(card)
        
        # Runs an extra time to ensure set returns to index 0
        # after going through full set.
        for num in range(0, 4):
            check = cards.get_top_card()
            self.assertEqual(check, to_add[num % 3])
    
    def test_study_set_get_top_card_empty(self):
        """Test that StudySet's get_top_card method
           properly returns None when the set is empty.
        """
        cards = study_set.StudySet("My set")
        
        check = cards.get_top_card()
        self.assertIsNone(check)
    
    def test_study_set_sort_cards(self):
        """Test that StudySet's sort_cards method
           properly sorts cards in-set in ascending
           alphabetical order by front value.
        """
        to_add = [study_set.Card("b", "back"),
                  study_set.Card("a", "back"),
                  study_set.Card("d", "back"),
                  study_set.Card("c", "back")]
        sorted_cards = [to_add[1], to_add[0], to_add[3], to_add[2]]
        cards = study_set.StudySet("My set")
        for card in to_add:
            cards.add_card(card)
        
        cards.sort_cards()
        self.assertListEqual(cards.cards, sorted_cards)