"""Run all tests for Letquiz."""

import unittest
import sys, os

p = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, p + '/../')

# Test modules
import test_file_io
import test_quiz
import test_study_set
import test_utils


def suite():
    """Create test suite of various test cases."""
    suite = unittest.TestSuite()
    loader = unittest.TestLoader()
    
    suite.addTests(loader.loadTestsFromModule(test_file_io))
    suite.addTests(loader.loadTestsFromModule(test_quiz))
    suite.addTests(loader.loadTestsFromModule(test_study_set))
    suite.addTests(loader.loadTestsFromModule(test_utils))
    
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())