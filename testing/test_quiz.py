"""Contains class to test data.quiz module."""

import unittest
import data.study_set as study_set
import data.quiz as quiz


class TestQuiz(unittest.TestCase):
    """Test the methods in the data.quiz module."""
    
    def setUp(self):
        """Create study set to use for quizzes."""
        to_add = [study_set.Card("front1", "back1"),
                  study_set.Card("front2", "back2"),
                  study_set.Card("front3", "back3")]
        self.card_set = study_set.StudySet("My set")
        for card in to_add:
            self.card_set.add_card(card)
    
    def test_quiz_init(self):
        """Test that Quiz init properly sets attributes."""
        test = quiz.Quiz(self.card_set, True)
        self.assertEqual(test.card_set, self.card_set)
        self.assertEqual(test.number_correct, 0)
        self.assertEqual(test.number_tested, 0)
        self.assertTrue(test.front_is_question)
    
    def test_quiz_get_question_front(self):
        """Test that Quiz's get_question method properly
           returns the front of the current card when
           the front is set to be the question.
        """
        test = quiz.Quiz(self.card_set, True)
        question = test.get_question()
        self.assertEqual(self.card_set.cards[0].front, question)
    
    def test_quiz_get_question_back(self):
        """Test that Quiz's get_question method properly
           returns the back of the current card when
           the back is set to be the question.
        """
        test = quiz.Quiz(self.card_set, False)
        question = test.get_question()
        self.assertEqual(self.card_set.cards[0].back, question)
        
    def test_quiz_get_question_none(self):
        """Test that Quiz's get_question method properly
           returns None when the quiz is over.
        """
        test = quiz.Quiz(self.card_set, True)
        test.number_tested = 3
        question = test.get_question()
        self.assertIsNone(question)
    
    def test_quiz_get_answer_front(self):
        """Test that Quiz's get_answer method properly
           returns the front of the current card when
           the back is set to be the question.
        """
        test = quiz.Quiz(self.card_set, False)
        answer = test.get_answer()
        self.assertEqual(self.card_set.cards[0].front, answer)
    
    def test_quiz_get_answer_back(self):
        """Test that Quiz's get_answer method properly
           returns the back of the current card when
           the front is set to be the question.
        """
        test = quiz.Quiz(self.card_set, True)
        answer = test.get_answer()
        self.assertEqual(self.card_set.cards[0].back, answer)
    
    def test_quiz_get_answer_none(self):
        """Test that Quiz's get_answer method properly
           returns None when the quiz is over.
        """
        test = quiz.Quiz(self.card_set, True)
        test.number_tested = 3
        answer = test.get_answer()
        self.assertIsNone(answer)
    
    def test_quiz_submit_answer_correct(self):
        """Test that Quiz's submit_answer method properly
           scores a correct answer, progresses with the quiz,
           and returns true.
        """
        test = quiz.Quiz(self.card_set, True)
        result = test.submit_answer(self.card_set.cards[0].back)
        self.assertTrue(result)
        self.assertEqual(test.number_tested, 1)
        self.assertEqual(test.number_correct, 1)
    
    def test_quiz_submit_answer_incorrect(self):
        """Test that Quiz's submit_answer method properly
           scores an incorrect answer, progresses with the quiz,
           and returns false.
        """
        test = quiz.Quiz(self.card_set, True)
        result = test.submit_answer("bad")
        self.assertFalse(result)
        self.assertEqual(test.number_tested, 1)
        self.assertEqual(test.number_correct, 0)
    
    def test_quiz_submit_answer_over(self):
        """Test that Quiz's submit_answer method properly
           does nothing when the quiz is over.
        """
        test = quiz.Quiz(self.card_set, True)
        test.number_tested = 3
        result = test.submit_answer("answer")
        self.assertIsNone(result)
        self.assertEqual(test.number_tested, 3)
        self.assertEqual(test.number_correct, 0)