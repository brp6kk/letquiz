"""Contains class to test data.file_io module."""

import unittest
import data.file_io as file_io

class TestFileIO(unittest.TestCase):
    """Test the methods in the data.file_io module."""
    
    def test_load_study_set(self):
        """Test that load_study_set properly reads a file
           and converts its contents to a study set.
        """
        cards = file_io.load_study_set("My Set")
        self.assertEqual(cards.name, "My Set")
        self.assertEqual(len(cards.cards), 1)
        self.assertEqual(cards.cards[0].front, "Front")
        self.assertEqual(cards.cards[0].back, "Back")