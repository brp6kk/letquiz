"""Run Letquiz program.
   Author: Eric Y. Chang
   Date: 2020/09/05
"""

import data.ui as ui


def main():
    """Entry point for Letquiz."""
    execute = True
    while(execute):
        execute = ui.start_up()
    
    print("Goodbye!")


if __name__ == '__main__':
    main()