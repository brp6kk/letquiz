"""Contains various helper methods."""

import data.file_io as file_io
import data.study_set as study_set


def get_json_files(files):
    """Go through a list of file names and build a new list
       containing only json file names, returning the new list.
    """
    json_files = [ file_name for file_name in files 
                   if file_name[-5:] == ".json" ]
    return json_files

def study_set_name_list(files = None):
    """Convert a list of files to a list of study set names.
       If files list is None, uses list of files in 
       Letquiz's study_sets directory.
    """
    if not files:
        files = file_io.study_set_file_list()
        
    files = get_json_files(files)
    study_sets = []
    
    # Remove ".json", change any "_" to " ".
    for file_name in files:
        name = file_name[0:-5]
        name = name.replace("_", " ")
        study_sets.append(name)
        
    return study_sets

def card_list_to_dictionary(cards):
    """Convert a list of cards to a dictionary,
       where the front is the key and the back is the value.
    """
    dictionary = {}
    for card in cards:
        dictionary[card.front] = card.back
    return dictionary

def dictionary_to_card_list(dictionary):
    """Convert a dictionary to a list of cards,
       where the key is the front and the back is the value.
    """
    card_list = []
    for entry in dictionary:
        card_list.append(study_set.Card(entry, dictionary[entry]))
    return card_list

def json_filename(name):
    """Add a directory and json file extension to parameter."""
    filename = name.replace(" ", "_")
    filename = "study_sets/" + filename + ".json"
    return filename