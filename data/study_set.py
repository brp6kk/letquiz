"""Contains Card and StudySet classes.
   Card represents a single card,
   StudySet represents a set of cards.
"""

import random


class Card():
    """Represents aspects of a flashcard."""
    
    def __init__(self, front, back):
        """Initialize card."""
        self.front = front
        self.back = back
    
    def to_string(self):
        value = ("Front: " + self.front + "; " +
                 "Back: " + self.back)
        return value


class StudySet():
    """Represents a set of cards."""
    
    def __init__(self, name):
        """Initialize study set."""
        self.name = name
        self.cards = []
        self.current_card_index = None
    
    def add_card(self, card):
        """Add a card to a study set."""
        self.cards.append(card)
        
        # Set index once first card is added.
        if self.current_card_index == None:
            self.current_card_index = 0
    
    def edit_card(self, old_card, new_card):
        """Replace old card with new card in the set.
           Do nothing if old card is not in the set.
        """
        try:
            self.remove_card(old_card)
        except ValueError: pass
        else:
            self.add_card(new_card)
    
    def remove_card(self, card):
        """Remove a card from a study set."""
        self.cards.remove(card)
        
        # When a list is made empty, 
        # there cannot be a "current card."
        if len(self.cards) == 0:
            self.current_card_index = None
            
    def find_card(self, front = "", back = ""):
        """Find and return a card either based on 
           front or back value.
        """
        if front == "" and back == "":
            return None
        
        for card in self.cards:
            if ((front != "" and card.front == front) or
                (back != "" and card.back == back)):
                return card
        
        return None
    
    def get_top_card(self):
        """Return current card, 
           then moves it to the bottom of the set.
        """
        if self.current_card_index == None:
            return None
        
        card = self.cards[self.current_card_index]
        
        self.current_card_index += 1
        if (self.current_card_index >= len(self.cards)):
            self.current_card_index = 0
        
        return card
    
    def sort_cards(self):
        """Sorts card in the set in ascending alphabetical order 
           based on each card's front.
        """
        self.cards.sort(key=lambda c : c.front)
    
    def shuffle_cards(self):
        """Randomizes order of cards in the set.
        """
        random.shuffle(self.cards)