"""Contains methods to interact with external files,
   which are used to save study sets.
"""

import os
import json
import data.study_set as study_set
import data.utils as utils


def study_set_file_list():
    """Return a list of all existing study sets."""
    p = os.path.dirname(os.path.abspath(__file__))
    p = p + "/../study_sets/"
    try:
        files = os.listdir(p)
    except OSError:
        return []
    else:
        return files

def save_study_set(cards):
    """Save a study set to a json file."""
    filename = utils.json_filename(cards.name)
    dictionary = utils.card_list_to_dictionary(cards.cards)
    
    with open(filename, 'w') as write_file:
        json.dump(dictionary, write_file)

def load_study_set(set_name):
    """Load a study set from a json file."""
    filename = utils.json_filename(set_name)
    if not os.path.exists(filename):
        return None
    
    with open(filename, 'r') as read_file:
        dictionary = json.load(read_file)
    
    card_list = utils.dictionary_to_card_list(dictionary)
    
    set_cards = study_set.StudySet(set_name)
    for entry in card_list:
        set_cards.add_card(entry)
        
    return set_cards

def delete_study_set(set_name):
    """Delete a study set."""
    filename = utils.json_filename(set_name)
    if os.path.exists(filename):
        os.remove(filename)