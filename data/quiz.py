"""Contains Quiz class.
   Quiz represents a single quiz, used to review a study set.
"""

import data.study_set as study_set


class Quiz():
    """Represents a quiz based on a set of cards."""
    
    def __init__(self, card_set, front_is_question):
        """Initialize quiz."""
        self.card_set = card_set
        self.number_correct = 0
        self.number_tested = 0
        self.front_is_question = front_is_question
    
    def get_question(self):
        """Return the current question,
           or return None if the quiz is over.
        """
        if self.is_quiz_over():
            return None
        
        current = self.card_set.cards[self.number_tested]
        if self.front_is_question:
            return current.front
        else:
            return current.back
    
    def get_answer(self):
        """Return the current answer,
           or return None if the quiz is over.
        """
        if self.is_quiz_over():
            return None
        
        current = self.card_set.cards[self.number_tested]
        if self.front_is_question:
            return current.back
        else:
            return current.front
    
    def submit_answer(self, answer):
        """Compare the submitted answer to the real answer.
           Return true if they are identical,
           return false if provided answer is not 
           the same as real answer.
           Additionally, moves on to the next question.
           Take no action and return None if the quiz is over.
        """
        if self.is_quiz_over():
            return None
        
        current = self.card_set.cards[self.number_tested]
        self.number_tested += 1
        # Compare answer to back if the front is the question
        # or compare answer to front if the back is the question.
        if ((self.front_is_question and current.back == answer) or
                (not self.front_is_question and current.front == answer)):
            self.number_correct += 1
            return True
        else:
            return False
    
    def is_quiz_over(self):
        """Return true if the quiz is over."""
        return self.number_tested >= len(self.card_set.cards)