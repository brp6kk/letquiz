"""Contains methods used to create the user interface
   and handle user input.
"""

import data.file_io as file_io
import data.quiz as quiz
import data.study_set as study_set
import data.utils as utils
from data.ui_helper import *


def start_up():
    """Prompt user on what task they wish to do,
       then allowing user to accomplish said task.
    """
    message = ("Welcome to Letquiz!\n\n" +
               " 1. Create New Study Set\n" +
               " 2. Edit a Study Set\n" +
               " 3. Delete a Study Set\n" +
               " 4. Quiz Setup\n" +
               " 5. Quit\n\n" +
               "Please enter the number corresponding with " +
               "the action you wish to take.")
    prompt = "What would you like to do?: "
    print(message)
    choice = input_int_range(prompt, 1, 6)
    print_separater()
    
    switcher = {
        1: create_study_set,
        2: edit_study_set,
        3: delete_study_set,
        4: quiz_setup,
    }
    func = switcher.get(choice, None)
    
    # User entered 5 - stop program.
    if not func:
        return 0
    
    # Continue program after executing action.
    func()
    return 1

def create_study_set():
    """Prompt user to name and add terms to a new study set."""
    name = None
    while not name:
        value = input("Enter the name for your new set: ")
        if (value in utils.study_set_name_list()):
            print("Sorry, a set with this name already exists.")
        else:
            name = value
    
    print_separater()
    print("Enter values for the front and back of each card.\n" +
          "Enter \"q\" at any time to quit.")
    
    cards = study_set.StudySet(name)
    while True:
        front = input("Front: ")
        if front.lower() == "q":
            break
        back = input("Back: ")
        if back.lower() == "q":
            break
            
        new_card = study_set.Card(front, back)
        cards.add_card(new_card)
        
        print()
        
    file_io.save_study_set(cards)
    print("Study set saved successfully!")
    
    print_separater()
    
def edit_study_set():
    """Prompt user to choose a study set to edit,
       then choose terms and edit them.
    """
    study_sets = check_for_study_sets()
    if not study_sets:
        return
    
    # Get study set to edit.
    prompt = ("Please enter the number corresponding with " +
              "the study set you wish to edit.")
    index = input_int_numbered_list(prompt, study_sets)
    print_separater()
    if index == -1:
        return
    
    cards = file_io.load_study_set(study_sets[index])
    # Edit cards.
    while True:
        # Get card to edit.
        prompt = ("Please enter the number corresponding with " +
                  "the card you wish to edit.")
        card_string_list = [ card.to_string() for card in cards.cards ]
        index = input_int_numbered_list(prompt, card_string_list)
        print_separater()
        if index == -1:
            file_io.save_study_set(cards)
            break
        
        # Edit card.
        print(cards.cards[index].to_string())
        print("Enter the new values for the card, " +
              "or simply press [enter] to not edit the value.")
        front = input("Front: ")
        back = input("Back: ")
        if front != "":
            cards.cards[index].front = front
        if back != "":
            cards.cards[index].back = back
        
        print_separater()

def delete_study_set():
    """Prompt user to choose a study set to remove, then remove it."""
    study_sets = check_for_study_sets()
    if not study_sets:
        return
    
    # Get study set to delete.
    prompt = ("Please enter the number corresponding with " +
              "the study set you wish to delete.")
    index = input_int_numbered_list(prompt, study_sets)
    print_separater()
    if index == -1:
        return
    
    # Delete study set.
    file_io.delete_study_set(study_sets[index])
    print("Study set deleted successfully.")
    print_separater()
    

def quiz_setup():
    """Prompt user to choose settings for a quiz."""
    study_sets = check_for_study_sets()
    if not study_sets:
        return
    
    # Get study set to test and shuffles cards.
    prompt = ("Please enter the number corresponding with " +
              "the study set you wish to study.")
    index = input_int_numbered_list(prompt, study_sets)
    print_separater()
    if index == -1:
        return
    card_set = file_io.load_study_set(study_sets[index])
    card_set.shuffle_cards()
    
    # Get side to test and creates quiz.
    prompt = ("Please enter the number corresponding with " +
              "what side of the flashcards you wish to be " +
              "the test question.\nYou will be expected to " +
              "provide the value of the other side as the answer.")
    choices = ["Front", "Back"]
    index = input_int_numbered_list(prompt, choices)
    print_separater()
    if index == -1:
        return
    test = quiz.Quiz(card_set, not index)
    
    # Get the kind of test and begins quiz.
    prompt = ("Please enter the number corresponding with " +
              "the type of test you wish to take.")
    choices = ["Flashcards", "Written"]
    index = input_int_numbered_list(prompt, choices)
    print_separater()
    if index == -1:
        return
    elif choices[index] == "Flashcards":
        flashcards_quiz(test)
    elif choices[index] == "Written":
        written_quiz(test)

def flashcards_quiz(test):
    """Go through a flashcards quiz."""
    while not test.is_quiz_over():
        # Show question.
        print("Question " + str(test.number_tested + 1) + " of " + 
              str(len(test.card_set.cards)) + ":")
        print(test.get_question() + "\n")
        input("Press [enter] to see the answer.")
        
        # Show answer.
        print(test.get_answer() + "\n")
        input("Press [enter] to continue.")
        print_separater()
        test.submit_answer("")
    
    print("Congratulations, you have finished studying this set!")
    input("Press [enter] to continue.")
    print_separater()

def written_quiz(test):
    """Go through a written quiz."""
    while not test.is_quiz_over():
        # Show question.
        print("Question " + str(test.number_tested + 1) + " of " + 
              str(len(test.card_set.cards)) + ":")
        print(test.get_question() + "\n")
        
        # Obtain answer.
        user_answer = input("Answer: ")
        real_answer = test.get_answer()
        
        # Tell user whether or not they are correct.
        if test.submit_answer(user_answer):
            print("Correct!")
        else:
            print("Sorry, the correct answer is:")
            print(real_answer)
        input("\nPress [enter] to continue.")
        print_separater()
    
    print("Congratulations, you have finished studying this set!\n" +
          "Score: " + str(test.number_correct) + " out of " + 
          str(test.number_tested))
    input("\nPress [enter] to continue.")
    print_separater()