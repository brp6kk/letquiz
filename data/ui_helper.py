"""Contains helper methods used to with 
   handling of the user interface.
"""

import data.utils as utils


def print_separater():
    """Add several newlines to separate parts of the UI."""
    for num in range(0, 3):
        print()

def print_list_prompt(string_list):
    """Print a list of strings, with each 
       string preceded by a numeral.
    """
    output = ""
    for num in range(0, len(string_list)):
        output += str(num + 1) + ". " + string_list[num] + "\n"
    output = output[0:-1]
    print(output)

def check_for_study_sets():
    """Check that at least one study set exists.
       If so, print nothing and return list of study set names.
       If none exist, print an error message and return None.
    """
    study_sets = utils.study_set_name_list()
    if len(study_sets) == 0:
        print("No study sets saved.")
        print_separater()
        return None
    else:
        return study_sets

def input_int(prompt):
    """Obtain integer value from user, 
       re-prompting them if they do not enter an integer.
    """
    while(True):
        try:
            num = (int(input(prompt)))
        except ValueError:
            print("Please enter an integer.")
        else:
            return num

def input_int_range(prompt, start, end):
    """Obtain integer value in specified range from user,
       re-prompting them if they do not enter a valid integer.
    """
    while(True):
        num = input_int(prompt)
        if (num < start or num >= end):
            print("Please enter an integer between " + str(start) + 
                  " and " + str(end - 1) + ".")
        else:
            return num

def input_int_numbered_list(prompt, string_list):
    """Print a list of strings, ten strings at a time,
       followed by "next" and "quit" prompts.
       Request the user enter a number corresponding
       to a string.
       Return either the index corresponding to
       the selected string, or -1 for "quit."
    """
    page = 0
    num = 0
    print(prompt)
    
    # Loop until user chooses a string
    # or until user chooses "quit."
    while True:
        print_list_prompt(string_list[page * 10:(page * 10) + 10])
        print("11. See more options")
        print("12. Quit\n")
        num = input_int_range("Enter a number: ", 1, 13)
        
        if num == 11:
            page += 1
            if (page * 10) > len(string_list):
                page = 0
        elif num == 12:
            num = -1
            break
        else:
            # Calculate number corresponding to list index.
            num = (num - 1) + (page * 10)
            # Last page may not have some valid integer choices.
            if num >= len(string_list):
                print_separater()
                print("Please enter a valid integer.")
            else:
                break
            
        print_separater()
    
    return num